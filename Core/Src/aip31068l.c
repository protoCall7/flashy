#include "aip31068l.h"

void
AIP31068_init(AIP31068 *dev,
		I2C_HandleTypeDef *i2chandle,
		enum AIP31068_LineMode n,
		enum AIP31068_FontSize f,
		enum AIP31068_Display d,
		enum AIP31068_Cursor c,
		enum AIP31068_Blink b,
		enum AIP31068_IncDec id,
		enum AIP31068_Shift sh)
{
	dev->i2chandle = i2chandle;
	dev->n = n;
	dev->f = f;
	dev->d = d;
	dev->c = c;
	dev->b = b;
	dev->id = id;
	dev->sh = sh;

	/* wait more that 15ms after Vcc */
	HAL_Delay(15);
	AIP31068_function_set(dev);

	HAL_Delay(1);
	AIP31068_display_control(dev);

	HAL_Delay(1);
	AIP31068_display_clear(dev);

	HAL_Delay(1);
	AIP31068_entry_mode_set(dev);
}

void
AIP31068_write_char(AIP31068 *dev, char c)
{
	uint8_t data[2];
	data[0] = AIP31068_DATA;
	data[1] = c;

	AIP31068_write_register(dev, data, 2);
}

void
AIP31068_write_string(AIP31068 *dev, char *str)
{
	for (int i = 0; str[i] != '\0'; i++) {
		AIP31068_write_char(dev, str[i]);
	}
}

void
AIP31068_set_pos(AIP31068 *dev, uint8_t col, uint8_t row)
{
	col = (row == 0 ? col|0x80 : col|0xc0);
	uint8_t data[2] = {AIP31068_COMMAND, col};

	AIP31068_write_register(dev, data, 2);
}

HAL_StatusTypeDef
AIP31068_function_set(AIP31068 *dev)
{
	uint8_t command[2];
	command[0] = AIP31068_COMMAND;
	command[1] = AIP31068_FUNCTION_SET;
	command[1] |= (dev->n << AIP31068_FUNC_LINES);
	command[1] |= (dev->f << AIP31068_FUNC_FONT);

	return AIP31068_write_register(dev, command, 2);
}

HAL_StatusTypeDef
AIP31068_display_control(AIP31068 *dev)
{
	uint8_t command[2];
	command[0] = AIP31068_COMMAND;
	command[1] = AIP31068_DISP_ON_OFF;
	command[1] |= (dev->d << AIP31068_DISP_CONTROL);
	command[1] |= (dev->c << AIP31068_DISP_CURS);
	command[1] |= (dev->b << AIP31068_DISP_BLINK);

	return AIP31068_write_register(dev, command, 2);
}

HAL_StatusTypeDef
AIP31068_display_clear(AIP31068 *dev)
{
	uint8_t command[2];
	command[0] = AIP31068_COMMAND;
	command[1] = AIP31068_CLR_DISP;

	return AIP31068_write_register(dev, command, 2);
}

HAL_StatusTypeDef
AIP31068_entry_mode_set(AIP31068 *dev)
{
	uint8_t command[2];
	command[0] = AIP31068_COMMAND;
	command[1] = AIP31068_ENTRY_MODE_SET;
	command[1] |= (dev->id << AIP31068_ENTRY_MODE_ID);
	command[1] |= (dev->sh << AIP31068_ENTRY_MODE_SH);

	return AIP31068_write_register(dev, command, 2);
}

HAL_StatusTypeDef
AIP31068_read_register(AIP31068 *dev, uint8_t reg, uint8_t *data)
{
	return HAL_I2C_Mem_Read(dev->i2chandle, AIP31068_I2C_ADDR, reg, I2C_MEMADD_SIZE_8BIT, data, 1, HAL_MAX_DELAY);
}

HAL_StatusTypeDef
AIP31068_read_registers(AIP31068 *dev, uint8_t reg, uint8_t *data, uint8_t length)
{
	return HAL_I2C_Mem_Read(dev->i2chandle, AIP31068_I2C_ADDR, reg, I2C_MEMADD_SIZE_8BIT, data, length, HAL_MAX_DELAY);
}

HAL_StatusTypeDef
AIP31068_write_register(AIP31068 *dev, uint8_t *data, uint8_t size)
{
	return HAL_I2C_Master_Transmit(dev->i2chandle, AIP31068_I2C_ADDR, data, size, HAL_MAX_DELAY);
}
