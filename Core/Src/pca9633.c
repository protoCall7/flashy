#include <math.h>
#include <stdio.h>

#include "pca9633.h"

uint8_t
PCA9633_init(PCA9633 *dev, I2C_HandleTypeDef *i2chandle)
{
	dev->i2chandle = i2chandle;

	uint8_t m1 = 0x00;
	uint8_t m2 = 1 << PCA9633_MODE2_DMBLNK;
	uint8_t ledout = 0xFF;
	uint8_t grppwm = 0xFF;
	uint8_t grpfreq = 0x00;

	PCA9633_write_register(dev, PCA9633_MODE1, &m1);
	PCA9633_write_register(dev, PCA9633_MODE2, &m2);
	PCA9633_write_register(dev, PCA9633_LEDOUT, &ledout);
	
	/* no blinky */
	PCA9633_write_register(dev, PCA9633_GRPPWM, &grppwm);
	PCA9633_write_register(dev, PCA9633_GRPFREQ, &grpfreq);

	return 0;
}

uint8_t
PCA9633_linearize(uint8_t n)
{
	return (log(n)/log(255)) * 255;
}

void
PCA9633_setrgba(PCA9633 *dev, uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	uint8_t lr = PCA9633_linearize(r);
	uint8_t lb = PCA9633_linearize(b);
	uint8_t lg = PCA9633_linearize(g);
	uint8_t la = PCA9633_linearize(a);

	printf("%d, %d, %d, %d\n", lr, lb, lg, la);

	PCA9633_write_register(dev, PCA9633_PWM0, &lb);
	PCA9633_write_register(dev, PCA9633_PWM1, &lg);
	PCA9633_write_register(dev, PCA9633_PWM2, &lr);
	PCA9633_write_register(dev, PCA9633_PWM3, &la);
}

HAL_StatusTypeDef
PCA9633_read_register(PCA9633 *dev, uint8_t reg, uint8_t *data)
{
	return HAL_I2C_Mem_Read(dev->i2chandle, PCA9633_I2C_ADDR, reg, I2C_MEMADD_SIZE_8BIT, data, 1, HAL_MAX_DELAY);
}

HAL_StatusTypeDef
PCA9633_read_registers(PCA9633 *dev, uint8_t reg, uint8_t *data, uint8_t length)
{
	return HAL_I2C_Mem_Read(dev->i2chandle, PCA9633_I2C_ADDR, reg, I2C_MEMADD_SIZE_8BIT, data, length, HAL_MAX_DELAY);
}

HAL_StatusTypeDef
PCA9633_write_register(PCA9633 *dev, uint8_t reg, uint8_t *data)
{
	return HAL_I2C_Mem_Write(dev->i2chandle, PCA9633_I2C_ADDR, reg, I2C_MEMADD_SIZE_8BIT, data, 1, HAL_MAX_DELAY);
}
