#ifndef __AIP31068__
#define __AIP31068__

#include "stm32f3xx.h"

#define AIP31068_I2C_ADDR (0x3E << 1)

#define AIP31068_COMMAND	0x00
#define AIP31068_DATA		0x40

/* commands */
#define AIP31068_CLR_DISP	0x001
#define AIP31068_RET_HOME	0x002
#define AIP31068_ENTRY_MODE_SET	0x004
#define AIP31068_DISP_ON_OFF	0x008
#define AIP31068_CURS_DISP_SHFT 0x010
#define AIP31068_FUNCTION_SET	0x020
#define AIP31068_SET_CGRAM_ADDR 0x040
#define AIP31068_SET_DDRAM_ADDR 0x080
#define AIP31068_WRITE_DATA	0x100

#define AIP31068_ENTRY_MODE_ID	0x1
#define AIP31068_ENTRY_MODE_SH  0x0
#define AIP31068_DISP_CONTROL	0x2
#define AIP31068_DISP_CURS	0x1
#define AIP31068_DISP_BLINK	0x0
#define AIP31068_FUNC_LINES	0x3
#define AIP31068_FUNC_FONT	0x2


enum AIP31068_LineMode {
	ONELINE,
	TWOLINE
};

enum AIP31068_FontSize {
	FIVE8,
	FIVE11
};

enum AIP31068_Display {
	DISPLAYOFF,
	DISPLAYON
};

enum AIP31068_Cursor {
	CURSOROFF,
	CURSORON
};

enum AIP31068_Blink {
	BLINKOFF,
	BLINKON
};

enum AIP31068_IncDec {
	DECREMENT,
	INCREMENT
};

enum AIP31068_Shift {
	SHIFT,
	NOSHIFT
};

typedef struct {
	I2C_HandleTypeDef *i2chandle;
	enum AIP31068_LineMode n;
	enum AIP31068_FontSize f;
	enum AIP31068_Display d;
	enum AIP31068_Cursor c;
	enum AIP31068_Blink b;
	enum AIP31068_IncDec id;
	enum AIP31068_Shift sh;
} AIP31068;

void AIP31068_init(AIP31068 *,
		I2C_HandleTypeDef *,
		enum AIP31068_LineMode,
		enum AIP31068_FontSize,
		enum AIP31068_Display,
		enum AIP31068_Cursor,
		enum AIP31068_Blink,
		enum AIP31068_IncDec,
		enum AIP31068_Shift);
void AIP31068_write_char(AIP31068 *, char);
void AIP31068_set_pos(AIP31068 *, uint8_t, uint8_t);
void AIP31068_write_string(AIP31068 *, char *);
HAL_StatusTypeDef AIP31068_entry_mode_set(AIP31068 *);
HAL_StatusTypeDef AIP31068_function_set(AIP31068 *);
HAL_StatusTypeDef AIP31068_display_control(AIP31068 *);
HAL_StatusTypeDef AIP31068_display_clear(AIP31068 *);
HAL_StatusTypeDef AIP31068_read_register(AIP31068 *, uint8_t, uint8_t *);
HAL_StatusTypeDef AIP31068_read_registers(AIP31068 *, uint8_t, uint8_t *, uint8_t);
HAL_StatusTypeDef AIP31068_write_register(AIP31068 *, uint8_t *, uint8_t);

#endif
